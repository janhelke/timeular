<?php

declare(strict_types=1);

namespace App\Providers;

use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;

class TimeularApiProvider
{
    protected string $token = '';

    protected Client $client;

    public function __construct()
    {
        $this->client = new Client(['http_errors' => false, 'verify' => false]);
        $this->signIn();
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public function signIn(): void
    {
        $response = $this->client->request(
            'POST',
            'https://' . getenv('TIMEULAR_API_URL') . '/developer/sign-in',
            [
                'json' => [
                    'apiKey' => getenv('TIMEULAR_API_KEY'),
                    'apiSecret' => getenv('TIMEULAR_API_SECRET'),
                ],
            ]
        );
        $this->token = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR)['token'];
    }

    /**
     * @return array|mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    public function devices(): array
    {
        $response = $this->client->request(
            'GET',
            'https://' . getenv('TIMEULAR_API_URL') . '/devices',
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->token,
                ],
            ]
        );
        return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR) ?? [];
    }

    /**
     * @return array|mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getTimeEntries(DateTime $from, DateTime $to): array
    {
        $response = $this->client->request(
            'GET',
            'https://' . getenv('TIMEULAR_API_URL') . '/time-entries/' . $this->formatDateTime($from) . '/' . $this->formatDateTime($to),
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->token,
                ],
            ]
        );
        $timeEntries = [];
        foreach (json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR)['timeEntries'] as $timeEntry) {
            $timeEntries[$timeEntry['duration']['startedAt']] = $timeEntry;
        }

        ksort($timeEntries);
        return  $timeEntries;
    }

    /**
     * @return array|mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    public function report(DateTime $from, DateTime $to): array
    {
        $response = $this->client->request(
            'GET',
            'https://' . getenv('TIMEULAR_API_URL') . '/report/' . $this->formatDateTime($from) . '/' . $this->formatDateTime($to) . '?timezone=Europe/Berlin',
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->token,
                ],
            ]
        );
        dd($response->getBody()->getContents());
        $report = array_map('str_getcsv', explode("\n", trim($response->getBody()->getContents())));
        array_walk($report, static function (&$a) use ($report): void {
            $a = array_combine($report[0], $a);
        });
        array_shift($report);
        return $report;
    }

    protected function formatDateTime(DateTime $dateTime): string
    {
        return $dateTime->format('Y-m-d\TH:i:s') . '.000';
    }
}

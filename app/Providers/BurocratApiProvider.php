<?php

declare(strict_types=1);

namespace App\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;

class BurocratApiProvider
{
    /**
     * Ablauf
     * Erst einen GET Request an "http://burocrat.f7net.de/workdays" senden.
     * Dabei muss der Cookie auth_token auf den Wert des Benutzers gesetzt werden.
     * Den auth_token bekommt man, wenn man sich regulär über den Browser anmeldet und dann in den Cookies nachschaut.
     *
     * Aus dem GET sollte man zum einen einen _burocrat_session Cookie bekommen als auch in den meta Tags einen gültigen
     * csrf-token
     *
     * Jetzt einen POST Request bauen:
     * URI:
     *  - http://burocrat.f7net.de/workdays
     *
     * Headers:
     *  - X-CSRF-Token = Der csrf-token aus den Metadaten
     *
     * Cookies:
     *  - auth_token = Siehe oben
     *  - _burocrat_session = Der Cookie, der vom GET Request geliefert wird.
     *
     * Form-Fields:
     *  - workday[started_at] = String "20/02/2021 10:56" muss gesetzt sein
     *  - workday[ended_at] = String "20/02/2021 10:57" kann gesetzt sein
     *  - workday[break_started_at] = String "20/02/2021 10:57" kann gesetzt sein
     *  - workday[break_ended_at] = String "20/02/2021 10:57" kann gesetzt sein
     */
    /**
     * @throws GuzzleException
     */
    public static function sendWorkhours(array $workday): void
    {
        $client = new Client(
            [
                'http_errors' => false,
                'cookies' => CookieJar::fromArray(
                    [
                        'auth_token' => getenv('BUROCRAT_AUTH_TOKEN'),
                    ],
                    getenv('BUROCRAT_API_URL')
                ),
            ]
        );

        $response = $client->request(
            'GET',
            'http://' . getenv('BUROCRAT_API_URL') . '/workdays',
        );
        $metaTags = self::extractMetaTags($response->getBody()->getContents());

        $client->request(
            'POST',
            'http://' . getenv('BUROCRAT_API_URL') . '/workdays',
            [
                'headers' => [
                    'X-CSRF-Token' => $metaTags['csrf-token']['value'],
                ],
                'form_params' => [
                    'workday' => $workday,
                ],
            ]
        );

        //@todo Funktionalität einbauen, um die ID des gerade erzeugten Eintrags auszulesen und zu speichern.
    }

    protected static function extractMetaTags(string $html): array
    {
        $metaTags = [];
        preg_match_all(
            '/<[\s]*meta[\s]*name="?([^>"]*)"?[\s]*content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si',
            $html,
            $match
        );

        if (isset($match) && is_array($match) && count($match) === 3) {
            [$originals, $names, $values] = $match;

            if (count($originals) === count($names) && count($names) === count($values)) {
                $metaTags = [];

                for ($i = 0, $limit = count($names); $i < $limit; ++$i) {
                    $metaTags[$names[$i]] = [
                        'html' => htmlentities($originals[$i]),
                        'value' => $values[$i],
                    ];
                }
            }
        }

        return $metaTags;
    }
}

<?php

declare(strict_types=1);

namespace App\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;

class RedmineApiProvider
{
    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public static function getTimeEntry(int $redmineTimeEntryId): array
    {
        $client = new Client(['http_errors' => false, 'verify' => false]);
        $response = $client->request(
            'GET',
            self::getServerCall() . 'time_entries/' . $redmineTimeEntryId . '.json'
        );
        $content = $response->getBody()->getContents();
        return $content === '' || $content === '0' ? [] : json_decode($content, true, 512, JSON_THROW_ON_ERROR)['time_entry'];
    }

    /**
     * @throws JsonException
     * @throws GuzzleException
     */
    public static function sendTimeEntry(array $timeEntry): array
    {
        $client = new Client(['http_errors' => false, 'verify' => false]);
        $response = $client->request(
            'POST',
            self::getServerCall() . 'time_entries.json',
            [
                'json' => [
                    'time_entry' => $timeEntry,
                ],
            ]
        );
        $content = $response->getBody()->getContents();
        return $content === '' || $content === '0' ? [] : json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @throws JsonException
     * @throws GuzzleException
     */
    public static function updateTimeEntry(int $redmineTimeEntryId, array $timeEntry)
    {
        $client = new Client(['http_errors' => false, 'verify' => false]);
        $response = $client->request(
            'PUT',
            self::getServerCall() . 'time_entries/' . $redmineTimeEntryId . '.json',
            [
                'json' => [
                    'time_entry' => $timeEntry,
                ],
            ]
        );
        $content = $response->getBody()->getContents();
        return $content === '' || $content === '0' ? [] : json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @throws GuzzleException
     * @throws JsonException
     */
    public static function getIssue(int $issueId): array
    {
        $client = new Client(['http_errors' => false, 'verify' => false]);
        $response = $client->request(
            'GET',
            self::getServerCall() . 'issues/' . $issueId . '.json'
        );
        $content = $response->getBody()->getContents();
        return $content === '' || $content === '0' ? [] : json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }

    protected static function getServerCall(): string
    {
        return
            'https://'
            . getenv('REDMINE_API_KEY') . ':' . getenv('REDMINE_API_SECRET')
            . '@'
            . getenv('REDMINE_API_URL')
            . '/';
    }
}

<?php

declare(strict_types=1);

namespace App\Providers;

class UrlaubsverwaltungApiProvider
{
    /**
     * Ablauf:
     *
     * Einen POST Request gegen https://urlaub.f7.de/login erzeugen:
     *  - username = LDAP Benutzer
     *  - password = LDAP Password
     *
     * Den Cookie JSESSIONID abgreifen
     *
     * Einen POST Request gegen https://urlaub.f7.de/web/overtime erzeugen:
     * Cookies:
     *  - JSESSIONID
     *
     * Felder:
     *  - person = Person ID im Tool
     *  - startDate = String "01.02.2021" muss gesetzt sein
     *  - endDate = String "07.02.2021" muss gesetzt sein
     *  - numberOfHours = Float "2.1" muss gesetzt sein
     * WARNUNG: Doppelte Requests werden doppelt eingetragen :-(
     * Deshalb muss die ID weggespeichert werden.
     *
     * Irgendwo im Quellcode steht folgendes drin:
     *
     * <a href="/web/overtime/1308/edit" class="fa-action pull-right" data-title="Editieren">
     *     <i class="fa fa-pencil"></i>
     * </a>
     *
     * Editieren läuft exakt so ab, nur dass noch das zusätzliche Feld
     *  - id = ID des Eintrags übergeben werden muss.
     */
}

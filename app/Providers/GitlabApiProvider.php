<?php

declare(strict_types=1);

namespace App\Providers;

use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Psr\Http\Message\ResponseInterface;

class GitlabApiProvider
{
    /**
     * @return array|mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    public static function getCommits(int $projectId, DateTime $since, DateTime $until, int $redmineIssueId = 0): array
    {
        $uri = 'https://' . getenv('GITLAB_API_URL') . '/api/v4/projects/' . $projectId . '/repository/commits';

        /**
         * As we need to assume that the commit wasn't merged into the default branch at this time, we check first
         * for the corresponding issue-branch.
         */
        if ($redmineIssueId > 0) {
            $response = self::getResponse($uri . '/issue-' . $redmineIssueId, $since, $until);
        } else {
            $response = self::getResponse($uri, $since, $until);
        }

        if ($response->getStatusCode() !== 200) {
            $response = self::getResponse($uri, $since, $until);
        }

        return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR) ?? [];
    }

    /**
     * @throws GuzzleException
     */
    protected static function getResponse(
        string $uri,
        DateTime $since,
        DateTime $until
    ): ResponseInterface {
        $client = new Client(
            [
                'http_errors' => false,
            ]
        );

        return $client->request(
            'GET',
            $uri,
            [
                'headers' => [
                    'PRIVATE-TOKEN' => getenv('GITLAB_API_TOKEN'),
                ],
                'form_params' => [
                    'since' => $since->sub(new \DateInterval('P120M'))->format('c'),
                    'until' => $until->add(new \DateInterval('P120M'))->format('c'),
                ],
            ]
        );
    }
}

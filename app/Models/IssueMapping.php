<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IssueMapping extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'timeular_id',
        'redmine_issue_id',
        'redmine_time_entry_id',
    ];
}

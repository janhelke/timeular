<?php

namespace App\Http\Controllers;

use App\Models\IssueMapping;
use App\Providers\BurocratApiProvider;
use App\Providers\GitlabApiProvider;
use App\Providers\RedmineApiProvider;
use App\Providers\TimeularApiProvider;
use DateTime;
use DateTimeZone;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\Redirector;
use JsonException;

class Controller extends BaseController
{
    protected array $redmineToGitlabMapping = [
        6 => 9, // KFC
        24 => 95, // Molkerei Weihenstephan
        51 => 140, //BNITM
        53 => 165, // Lange Nacht der Industrie
        56 => 74, // Dermalog
        62 => 262, // müllermilch.de
        72 => 235, // f7.de
        76 => 235, // f7.de
        96 => 133, // KFC Suisse
        97 => 276, // Muller UK
        113 => 95, // Molkerei Weihenstephan Laufende Wartung
        114 => 325,
        118 => 274, // Sachsenmilch.com
        133 => 192, // Lisner.pl
        148 => 0, //Comdirect finanzheldingen
        161 => 95, // Molkerei Weihenstephan SEO
        171 => 201, // Barkassen Meyer
        180 => 0, // F7 loves TYPO3
        190 => 145, // MMS
        226 => 208, // Muller EE
        231 => 216, // Lieken
        232 => 288,
        233 => 216, // Golden Toast
        234 => 216, // Lieken Urkorn
        236 => 250, // Muller NL
        242 => 0, // Voyager Orbit
        255 => 277, // Rainbow World
        256 => 227, // Rainbow World Sprint 1
        257 => 225, // Optipack
        259 => 0, // Apollo Orga
        262 => 0, // F7 Orga
        269 => 243, // T.M.A.
        271 => 244, // Aidshilfe Hamburg
        280 => 0, //Systemadministration
        301 => 225, //Optipack
        303 => 258, // gyn-werden
        304 => 270, // Pride
        309 => 276, // Muller UK
        318 => 312, // Muller Italy
        323 => 270, // Pride
        327 => 286, // DB Mobil
        333 => 0,
        334 => 257, // Gaia
        335 => 137, // Discovery
        336 => 276, // Muller UK
        337 => 313, // Plantopia
        356 => 208, // Muller EE
        357 => 142, // FTA
        360 => 312, // Muller Italy Maintainance
        361 => 74,
        362 => 74, // Dermalog Maintainance
        363 => 0,
        379 => 391, // Culina / Emhage
        380 => 390, // Leibniz IWT
        381 => 0,
        382 => 405, // Mecklenburger Radtour
        383 => 406, // Müllergroup
        390 => 0,
        391 => 74, // Dermalog Feedback Update
        394 => 419, // Leibniz-Institut für Virologie - LIV
        376 => 416, // Leoni
        392 => 416, // Leoni
        399 => 435, // Gropper
        404 => 431, // Upstalsboom
        413 => 466, // Kulturpalast Hamburg
    ];

    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * @return Application|Factory|View
     * @throws JsonException
     * @throws GuzzleException
     */
    public function welcome(string $ts = '', array $errors = [])
    {
        $timestamp = $ts === '' || $ts === '0' ? time() : (int)$ts;
        $timeularApi = new TimeularApiProvider();
        $from = new DateTime();
        $from->setDate(
            date('Y', $timestamp),
            date('m', $timestamp),
            date('d', $timestamp)
        );
        $from->setTime(0, 0);

        $to = new DateTime();
        $to->setDate(
            date('Y', $timestamp),
            date('m', $timestamp),
            date('d', $timestamp)
        );
        $to->setTime(23, 59, 59, 999);

        $timeularTimeEntries = $timeularApi->getTimeEntries($from, $to);
        if ($timeularTimeEntries !== []) {
            $redmineTimeEntries = [];
            $redmineWorkHoursSum = 0.0;
            $workMinutesSum = 0;

            $startOfDay = 0;

            foreach ($timeularTimeEntries as $runningNumber => $timeularEntry) {
                $redmineIssueIds = $this->guessRedmineIssueIdsFromTimeularTags($timeularEntry);
                $start = (new DateTime($timeularEntry['duration']['startedAt']))->setTimezone(new \DateTimeZone('Europe/Berlin'));
                $stop = (new DateTime($timeularEntry['duration']['stoppedAt']))->setTimezone(new \DateTimeZone('Europe/Berlin'));
                if ($startOfDay === 0) {
                    $startOfDay = $start;
                }

                $duration = (int)ceil(($stop->getTimestamp() - $start->getTimestamp()) / 60);

                if ($redmineIssueIds !== []) {
                    $quarterlyDuration = $this->calculateDecimalQuarterHours($duration / count($redmineIssueIds));
                    $redmineWorkHoursSum += (float)$quarterlyDuration;
                    foreach ($redmineIssueIds as $redmineIssueId) {
                        $redmineIssue = RedmineApiProvider::getIssue($redmineIssueId);
                        $redmineTimeEntryId = IssueMapping::all()
                            ->where('timeular_id', $timeularEntry['id'])
                            ->where('redmine_issue_id', $redmineIssueId)
                            ->first()->redmine_time_entry_id
                            ?? '';

                        $redmineTimeEntry = [];
                        if ($redmineTimeEntryId !== '') {
                            $redmineTimeEntry = RedmineApiProvider::getTimeEntry($redmineTimeEntryId);
                        }

                        $redmineTimeEntries[] = [
                            'timeular_id' => $timeularEntry['id'],
                            'redmine_time_entry_id' => $redmineTimeEntryId,
                            'date' => $start->format('Y-m-d'),
                            'duration' => $quarterlyDuration,
                            'real_duration' => $duration / count($redmineIssueIds),
                            'comment' => $this->getComment($redmineIssue['issue'] ?? [], $timeularEntry, $redmineTimeEntry),
                            'activity_id' => 9,
                            'accounting_id' => $this->guessAccountingId($redmineIssue['issue'] ?? [], $redmineTimeEntry),
                            'redmine_issue' => $redmineIssue['issue'] ?? [],
                            'errors' => $errors[$runningNumber] ?? [],
                        ];
                    }
                } else {
                    $quarterlyDuration = $this->calculateDecimalQuarterHours($duration);
                    $redmineWorkHoursSum += (float)$quarterlyDuration;

                    $redmineTimeEntries[] = [
                        'timeular_id' => $timeularEntry['id'],
                        'redmine_time_entry_id' => '',
                        'date' => $start->format('Y-m-d'),
                        'duration' => $quarterlyDuration,
                        'comment' => $this->getComment([], $timeularEntry, []),
                        'activity_id' => 9,
                        'accounting_id' => $this->guessAccountingId([], []),
                        'redmine_issue' => [],
                        'errors' => $errors[$runningNumber] ?? [],
                    ];
                }

                $workMinutesSum += $duration;
            }

            // Fake the break start and end to please the Steuerberaterin
            $breakStart = new DateTime($startOfDay->format('Y-m-d 12:' . random_int(0, 59) . ':00'), new DateTimeZone('Europe/Berlin'));
            $breakDuration = new \DateInterval('PT' . random_int(30, 60) . 'M');
            $burocratTimeEntry = [
                'start' => $startOfDay
                    ->format('d/m/Y H:i'),
                'breakStart' => $breakStart->format('d/m/Y H:i'),
                'breakEnd' => (clone $breakStart)->add($breakDuration)->format('d/m/Y H:i'),
                'end' => (clone $startOfDay)
                    ->add(new \DateInterval('PT' . $workMinutesSum . 'M'))
                    ->add($breakDuration)
                    ->format('d/m/Y H:i'),
            ];
        }

        $dates = [
            'lastWeek' => $timestamp - 604800,
            'lastDay' => $timestamp - 86400,
            'thisDay' => $timestamp,
            'nextDay' => $timestamp + 86400,
            'nextWeek' => $timestamp + 604800,
        ];

        return view(
            'welcome',
            [
                'timestamp' => $timestamp,
                'redmineTimeEntries' => $redmineTimeEntries ?? [],
                'dates' => $dates,
                'burocratTimeEntry' => $burocratTimeEntry ?? [],
            ]
        );
    }

    /**
     * @return Application|RedirectResponse|Redirector
     * @throws JsonException
     */
    public function submitRedmine(Request $request)
    {
        $variables = $request->all();
        $entries = $variables['entries'];
        $errors = [];
        for ($i = 0, $iMax = count($entries[array_key_first($entries)]); $i < $iMax; ++$i) {
            if ($entries['redmine_issue_id'][$i]) {
                $timeEntry = [
                    'issue_id' => (int)$entries['redmine_issue_id'][$i],
                    'spent_on' => $entries['date'][$i],
                    'hours' => (float)$entries['duration'][$i],
                    'activity_id' => (int)$entries['activity'][$i],
                    'custom_fields' => [
                        [
                            'value' => $entries['accounting'][$i],
                            'id' => 1,
                        ],
                    ],
                    'comments' => $entries['comment'][$i] ?? ' ',
                ];
                if (empty($entries['redmine_time_entry_id'][$i])) {
                    $redmineTimeEntry = RedmineApiProvider::sendTimeEntry($timeEntry);

                    if (!empty($redmineTimeEntry['errors'])) {
                        $errors[$i] = $redmineTimeEntry['errors'];
                    } elseif (!empty($redmineTimeEntry['time_entry']['id'])) {
                        $entry = new IssueMapping();
                        $entry->fill([
                            'timeular_id' => $entries['timeular_id'][$i],
                            'redmine_issue_id' => $entries['redmine_issue_id'][$i],
                            'redmine_time_entry_id' => $redmineTimeEntry['time_entry']['id'],
                        ]);

                        $entry->save();
                    }
                } else {
                    $redmineTimeEntry = RedmineApiProvider::updateTimeEntry(
                        (int)$entries['redmine_time_entry_id'][$i],
                        $timeEntry
                    );
                    if (!empty($redmineTimeEntry['errors'])) {
                        $errors[$i] = $redmineTimeEntry['errors'];
                    }
                }
            }
        }

        return redirect(route('start', ['timestamp' => $variables['timestamp'], 'errors' => $errors]));
    }

    /**
     * @return Application|RedirectResponse|Redirector
     * @throws JsonException
     */
    public function submitBurocrat(Request $request)
    {
        $variables = $request->all();
        BurocratApiProvider::sendWorkhours($variables['workday']);
        return redirect(route('start', ['timestamp' => $variables['timestamp']]));
    }

    /**
     * @return Application|RedirectResponse|Redirector
     * @throws JsonException
     */
    public function submitUrlaubsverwaltung(Request $request)
    {
        $variables = $request->all();
        return redirect(route('start', ['timestamp' => $variables['timestamp']]));
    }

    protected function calculateDecimalQuarterHours(string $duration): string
    {
        $hours = floor($duration / 60);
        $minutes = $duration - $hours * 60;

        $quarter = 0;
        // This is the equivalent of "The first 3 minutes are free".
        if ($minutes >= 3 && $minutes < 18) {
            $quarter = 0.25;
        } elseif ($minutes >= 18 && $minutes < 33) {
            $quarter = 0.5;
        } elseif ($minutes >= 33 && $minutes < 48) {
            $quarter = 0.75;
        } elseif ($minutes >= 48 && $minutes < 59.999) {
            ++$hours;
        }

        return (string)($hours + $quarter);
    }

    protected function calculateMinutes(string $duration): int
    {
        [$hours, $minutes, $seconds] = explode(':', $duration);
        if ($seconds > 30) {
            ++$minutes;
        }

        return $hours * 60 + $minutes;
    }

    protected function guessRedmineIssueIdsFromTimeularTags(array $timeEntry): array
    {
        $issue = [];
        foreach ($timeEntry['note']['tags'] as $tag) {
            if (is_numeric($tag['label'])) {
                $issue[] = (int)$tag['label'];
            }
        }

        if ($issue === [] && !empty($timeEntry['note']['text'])) {
            foreach (explode(',', $timeEntry['note']['text']) as $tag) {
                foreach (explode(' ', trim($tag)) as $word) {
                    if (strpos($word, '#') === 0) {
                        $issue[] = (int)substr($word, 1);
                    }
                }
            }
        }

        return $issue;
    }

    protected function guessAccountingId(array $redmineIssue, array $redmineTimeEntry): string
    {
        if (!empty($redmineTimeEntry['custom_fields'])) {
            foreach ($redmineTimeEntry['custom_fields'] as $custom_field) {
                if ($custom_field['name'] === 'Abrechnen') {
                    return $custom_field['value'];
                }
            }
        }

        if ($redmineIssue === []) {
            return '';
        }

        switch ((int)$redmineIssue['id']) {
            case 13087: // Daily, Weekly, Retro
                return 'TZ';
            case 11555: // Lean Coffee
            case 13306: // Social Time
            case 13222: // Knowledge Camp
                return 'OE';
            case 8892: // Jans Sammelticket
                return 'Nein';
        }

        return '';
    }

    /**
     * @throws JsonException
     * @throws GuzzleException
     */
    protected function getComment(array $redmineIssue, array $timeularEntry, array $redmineTimeEntry): string
    {
        $comment = $redmineTimeEntry['comments'] ?? '';
        if (trim($comment) === '' && !empty($redmineIssue['id'])) {
            $comment = $this->getCleanedTextFromTimeularEntry($timeularEntry);
            if ($comment === '') {
                $comment = $this->getCommentFromGitlab($redmineIssue, $timeularEntry);
            }
        }

        // Educational effort
        if (($redmineIssue['id'] ?? 0) === 22070 && str_starts_with($comment, '*')) {
            $educationalRedmineIssueId = (int)substr($comment, 1);
            $educationalRedmineIssue = RedmineApiProvider::getIssue($educationalRedmineIssueId);
            $comment = ($educationalRedmineIssue['issue']['project']['name'] ?? '') . ' - ' . $educationalRedmineIssueId . ' - ' . ($educationalRedmineIssue['issue']['subject'] ?? '');
        }

        return $comment;
    }

    protected function getCleanedTextFromTimeularEntry(array $timeularEntry): string
    {
        $comment = $timeularEntry['note']['text'];
        $comment = preg_replace('|<{{(.*?)}}>|su', '', $comment);
        return trim($comment);
    }

    /**
     * @throws JsonException
     * @throws GuzzleException
     */
    protected function getCommentFromGitlab(array $redmineIssue, array $timeularEntry): string
    {
        $commitMessages = [];
        $commits = GitlabApiProvider::getCommits(
            $this->redmineToGitlabMapping[(int)$redmineIssue['project']['id']],
            new DateTime(
                $timeularEntry['duration']['startedAt'],
                new \DateTimeZone('Europe/Berlin')
            ),
            new DateTime(
                $timeularEntry['duration']['stoppedAt'],
                new \DateTimeZone('Europe/Berlin')
            ),
            $redmineIssue['id']
        );

        foreach ($commits as $commit) {
            if (!empty($commit['message']) && strpos($commit['message'], '#' . $redmineIssue['id']) !== false) {
                $commitMessages[] = $commit['title'];
            }
        }

        return implode(', ', $commitMessages);
    }
}

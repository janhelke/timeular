<?php

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', static function (string $ts = '', array $errors = []): Application|Factory|View {
    return (new Controller())->welcome($ts, $errors);
})->name('start');
Route::get('/{timestamp}', static function (string $ts = '', array $errors = []): Application|Factory|View {
    return (new Controller())->welcome($ts, $errors);
})->name('start');
Route::post('/submitRedmine/', static function (Request $request): Application|RedirectResponse|Redirector {
    return (new Controller())->submitRedmine($request);
})->name('submitRedmine');
Route::post('/submitBurocrat/', static function (Request $request): Application|RedirectResponse|Redirector {
    return (new Controller())->submitBurocrat($request);
})->name('submitBurocrat');
Route::post('/submitUrlaubsverwaltung/', static function (Request $request): Application|RedirectResponse|Redirector {
    return (new Controller())->submitUrlaubsverwaltung($request);
})->name('submitUrlaubsverwaltung');

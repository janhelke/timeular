<h2>Redmine</h2>

<form action="{{ route('submitRedmine') }}" method="POST">
    @csrf
    <input type="hidden" name="timestamp" value="{{$timestamp}}">
    @foreach($timeEntries as $timeEntry)
        <div class="row py-1 {{$loop->odd ? 'bg-light' : ''}}">
            <input type="hidden" name="entries[timeular_id][]" value="{{$timeEntry['timeular_id']}}">
            <input type="hidden" name="entries[redmine_time_entry_id][]"
                   value="{{$timeEntry['redmine_time_entry_id']}}">
            <input type="hidden" name="entries[date][]" value="{{$timeEntry['date']}}">

            <div class="col-6">
                {{!empty($timeEntry['redmine_issue']['project']['name']) ? $timeEntry['redmine_issue']['project']['name'] . ': ' : ''}}{{$timeEntry['redmine_issue']['subject'] ?? ''}}
            </div>
            <div class="col-1">
                <input class="form-control" type="text" name="entries[redmine_issue_id][]"
                       value="{{$timeEntry['redmine_issue']['id'] ?? ''}}">
            </div>
            <div class="col-1">
                <input class="form-control" type="text" name="entries[duration][]"
                       value="{{$timeEntry['duration'] ?? ''}}">
            </div>
            <div class="col-2">
                <select class="form-control" name="entries[activity][]"
                        value="{{$timeEntry['duration'] ?? ''}}">
                    <option {{ $timeEntry['activity_id'] === 0 ? 'selected' : '' }} value="">
                        --- Bitte auswählen ---
                    </option>
                    <option {{ $timeEntry['activity_id'] === 8 ? 'selected' : '' }} value="8">
                        Design
                    </option>
                    <option {{ $timeEntry['activity_id'] === 9 ? 'selected' : '' }} value="9">
                        Entwicklung
                    </option>
                    <option {{ $timeEntry['activity_id'] === 10 ? 'selected' : '' }} value="10">
                        Projektmanagement
                    </option>
                    <option {{ $timeEntry['activity_id'] === 11 ? 'selected' : '' }} value="11">
                        Konzeption
                    </option>
                    <option {{ $timeEntry['activity_id'] === 12 ? 'selected' : '' }} value="12">
                        Administration
                    </option>
                    <option {{ $timeEntry['activity_id'] === 13 ? 'selected' : '' }} value="13">
                        Inhaltspflege
                    </option>
                </select>
            </div>
            <div class="col-2">
                <select class="form-control" name="entries[accounting][]"
                        value="{{$timeEntry['duration'] ?? ''}}">
                    <option {{ $timeEntry['accounting_id'] === '' ? 'selected' : '' }} value="">
                        --- Bitte auswählen ---
                    </option>
                    <option {{ $timeEntry['accounting_id'] === 'Ja' ? 'selected' : '' }} value="Ja">
                        Ja
                    </option>
                    <option {{ $timeEntry['accounting_id'] === 'Nein' ? 'selected' : '' }} value="Nein">
                        Nein
                    </option>
                    <option {{ $timeEntry['accounting_id'] === 'KVA' ? 'selected' : '' }} value="KVA">
                        KVA
                    </option>
                    <option
                        {{ $timeEntry['accounting_id'] === 'Wartung' ? 'selected' : '' }} value="Wartung">
                        Wartung
                    </option>
                    <option
                        {{ $timeEntry['accounting_id'] === 'Kontingent' ? 'selected' : '' }} value="Kontingent">
                        Kontingent
                    </option>
                    <option
                        {{ $timeEntry['accounting_id'] === 'Klärung' ? 'selected' : '' }} value="Klärung">
                        Klärung
                    </option>
                    <option
                        {{ $timeEntry['accounting_id'] === 'Ist abgerechnet' ? 'selected' : '' }} value="Ist abgerechnet">
                        Ist abgerechnet
                    </option>
                    <option {{ $timeEntry['accounting_id'] === 'AG' ? 'selected' : '' }} value="AG">
                        AG
                    </option>
                    <option {{ $timeEntry['accounting_id'] === 'TZ' ? 'selected' : '' }} value="TZ">
                        TZ
                    </option>
                    <option {{ $timeEntry['accounting_id'] === 'OE' ? 'selected' : '' }} value="OE">
                        OE
                    </option>
                    <option {{ $timeEntry['accounting_id'] === 'TYPO3-Community' ? 'selected' : '' }} value="TYPO3-Community">
                        TYPO3-Community
                    </option>
                    <option {{ $timeEntry['accounting_id'] === 'Fortbildung' ? 'selected' : '' }} value="Fortbildung">
                        Fortbildung
                    </option>
                    <option {{ $timeEntry['accounting_id'] === 'Akquise' ? 'selected' : '' }} value="Akquise">
                        Akquise
                    </option>
                </select>
            </div>

        </div>
        <div class="row py-1 {{$loop->odd ? 'bg-light' : ''}}">
            <div class="col-2">
                @if ($timeEntry['redmine_time_entry_id'])
                    <img src="{{ asset('images/redmine.svg') }}" height="38px" alt="redmine_time_entry_id: {{$timeEntry['redmine_time_entry_id']}}"/>
                @else
                    <img src="{{ asset('images/redmine_grey.svg') }}" height="38px"/>
                @endif

            </div>
            <div class="col-10">
                <input class="form-control"
                       type="text"
                       name="entries[comment][]"
                       value="{{$timeEntry['comment'] ?? ''}}"
                       placeholder="Comment"
                >
            </div>
        </div>
        @if (!empty($timeEntry['errors']))
            @foreach($timeEntry['errors'] as $error)
                <div class="row py-1 {{$loop->odd ? 'bg-light' : ''}}">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <div class="alert alert-danger" role="alert">
                            {{$error}}
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    @endforeach
    <div class="row py-3">
        <div class="col">
            <input type="submit" class="btn btn-success" value="Submit">
        </div>
    </div>
</form>

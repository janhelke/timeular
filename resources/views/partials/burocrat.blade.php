<h2>Burocrat</h2>

<form action="{{ route('submitBurocrat') }}" method="POST">
    @csrf
    <input type="hidden" name="timestamp" value="{{$timestamp}}">
    <div class="row py-1">
        <div class="col-3">
            Start
        </div>
        <div class="col-3">
            <input class="form-control" type="text" name="workday[started_at]"
                   value="{{$burocratTimeEntry['start'] ?? ''}}">
        </div>
        <div class="col-3">
            Ende
        </div>
        <div class="col-3">
            <input class="form-control" type="text" name="workday[ended_at]"
                   value="{{$burocratTimeEntry['end'] ?? ''}}">
        </div>
    </div>
    <div class="row py-1">
        <div class="col-3">
            Pause Start
        </div>
        <div class="col-3">
            <input class="form-control" type="text" name="workday[break_started_at]" value="{{$burocratTimeEntry['breakStart'] ?? ''}}">
        </div>
        <div class="col-3">
            Pause Ende
        </div>
        <div class="col-3">
            <input class="form-control" type="text" name="workday[break_ended_at]" value="{{$burocratTimeEntry['breakEnd'] ?? ''}}">
        </div>
    </div>

    <div class="row py-3">
        <div class="col">
            <input type="submit" class="btn btn-success" value="Submit">
        </div>
    </div>
</form>

<?php

declare(strict_types=1);


use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\SetList;
use Rector\ValueObject\PhpVersion;
use RectorLaravel\Set\LaravelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->sets([
        SetList::CODE_QUALITY,
        SetList::PHP_82,
        SetList::CODING_STYLE,
        SetList::DEAD_CODE,
        SetList::TYPE_DECLARATION,
        LaravelSetList::LARAVEL_80
    ]);

    $rectorConfig->importNames();
    $rectorConfig->disableParallel();
    $rectorConfig->importShortClasses(false);
    $rectorConfig->phpVersion(PhpVersion::PHP_82);

    $rectorConfig->paths([
        __DIR__ . '/../../app',
        __DIR__ . '/../../database',
        __DIR__ . '/../../resources',
        __DIR__ . '/../../routes',
        __DIR__ . '/../../tests',
    ]);
};

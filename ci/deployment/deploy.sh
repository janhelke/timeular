#!/usr/bin/env bash

TIMESTAMP=$(date +%Y%m%d%H%M%S)
echo "${CI_COMMIT_REF_SLUG}"
if [ "${CI_COMMIT_REF_SLUG}" == "develop" ] || [ "${CI_COMMIT_REF_SLUG}" == "main" ]; then
  STAGE_PATH=${SSH_PATH}/${CI_COMMIT_REF_SLUG}
else
  STAGE_PATH=${SSH_PATH}/${CI_COMMIT_REF_SLUG}-${PROJECT_NAME}
fi
echo "${STAGE_PATH}"
RELEASE=${CI_PIPELINE_ID}_${TIMESTAMP}
RELEASE_PATH=${STAGE_PATH}/releases/${RELEASE}

echo ">>>>> Create Release: $RELEASE <<<<<"
ssh -o StrictHostKeyChecking=no -p ${SSH_PORT} ${SSH_CON} "mkdir -p $RELEASE_PATH"
retval=$?

if [ $retval -eq 0 ]; then
  echo ">>>>> Replace path to .htpasswd <<<<<"
  PATH_TO_HTPASSWD_REPLACEMENT=${RELEASE_PATH}
  sed -i 's@###PATH_TO_HTPASSWD###@'"$PATH_TO_HTPASSWD_REPLACEMENT"'@g'  public/.htaccess
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo ">>>>> Rsync files <<<<<"
  rsync -rav -e "ssh -p $SSH_PORT" --include-from=ci/deployment/filestructure.rules . ${SSH_CON}:${RELEASE_PATH}/
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo ">>>>> Setup Laravel <<<<<"
  ssh -o StrictHostKeyChecking=no -p ${SSH_PORT} ${SSH_CON} "
  rm -rf $RELEASE_PATH/storage/app && \
  ln -sr $STAGE_PATH/shared/storage/app $RELEASE_PATH/storage && \
  cd $RELEASE_PATH && \
  $SSH_PHP artisan migrate --force && \
  $SSH_PHP artisan key:generate && \
  $SSH_PHP artisan cache:clear"
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo ">>>>> Release & link artisan <<<<<"
  ssh -o StrictHostKeyChecking=no -p ${SSH_PORT} ${SSH_CON} "
  ln -fsn $RELEASE_PATH/public $STAGE_PATH/current && \
  ln -fsn $RELEASE_PATH/artisan $STAGE_PATH/artisan
  "
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo ">>>>> Remove old releases <<<<<"
  ssh -o StrictHostKeyChecking=no -p ${SSH_PORT} ${SSH_CON} "
  cd $STAGE_PATH/releases/ && find -maxdepth 1 -type d ! -iname \"$RELEASE\" -exec rm -rf {} \;"
  warning=$((warning + $?))

  echo ">>>>> Cronjob path: $STAGE_PATH/artisan <<<<<"
  echo ">>>>> Deployment: $SSH_CON:$RELEASE_PATH <<<<<"
fi

exit $retval

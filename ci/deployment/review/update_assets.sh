#!/usr/bin/env bash

# This script syncs the database to the asset server

retval=$?

if [ $retval -eq 0 ]; then
  ssh -o StrictHostKeyChecking=no -A -p ${REVIEW_PORT} ${REVIEW_CON} "mkdir -p /home/review/assets/$PROJECT_NAME/"
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo "Dump structure"
  ssh -o StrictHostKeyChecking=no -A -p ${SSH_PORT} ${SSH_CON} "cd $SSH_PATH && mysqldump --host='$DB_HOST' --user='$DB_USER' --password='$DB_PASSWORD' --single-transaction --no-data --routines $DB_DBNAME > db.sql"
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  IGNORED_TABLES=""
  for TABLE in ${EXCLUDE_TABLES}; do
     IGNORED_TABLES="${IGNORED_TABLES} --ignore-table=$DB_DBNAME.$TABLE"
  done
  echo "Dump data with $IGNORED_TABLES"
  ssh -o StrictHostKeyChecking=no -A -p ${SSH_PORT} ${SSH_CON} "cd $SSH_PATH && mysqldump --host='$DB_HOST' --user='$DB_USER' --password='$DB_PASSWORD' $DB_DBNAME --no-create-info --max_allowed_packet=1073741824 --skip-triggers $IGNORED_TABLES >> db.sql"
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo "Sync db dump"
  ssh -o StrictHostKeyChecking=no -A -p ${REVIEW_PORT} ${REVIEW_CON} "rsync -avzh --omit-dir-times -e \"ssh -o StrictHostKeyChecking=no -A -p $SSH_PORT\" $SSH_CON:$SSH_PATH/db.sql /home/review/assets/$PROJECT_NAME"
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo "Remove dbdump"
  ssh -o StrictHostKeyChecking=no -A -p ${SSH_PORT} ${SSH_CON} "cd $SSH_PATH && rm db.sql"
  retval=$((retval + $?))
fi

return $retval
